# Builds zipped FB2 book

ZIP=zip

%.fb2.zip: %.fb2
	$(ZIP) $@ $<

dal_agoccie-Eng.fb2.zip: dal_agoccie-Eng.fb2

all: dal_agoccie-Eng.fb2.zip

