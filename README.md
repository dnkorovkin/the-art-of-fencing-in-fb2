# The Art of Fencing in FB2

This project aims to convert the original 2007 partial translation of Giovanni
Dall'agocchie's book Dell'Arte di Scrimia Libri Tre, translated by Jherek
Swanger, to FB2 and make it more mobile readers friendly. The original
translation may be found [on the translator's
homepage](http://celyn.drizzlehosting.com/jherek/ENGDALLAG.pdf).

The project does not aim to change the translation contents.

This project was undertaken with the translator's permission.
